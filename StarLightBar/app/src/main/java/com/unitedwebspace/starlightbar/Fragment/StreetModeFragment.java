package com.unitedwebspace.starlightbar.Fragment;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clj.fastble.conn.BleCharacterCallback;
import com.clj.fastble.exception.BleException;
import com.unitedwebspace.starlightbar.Base.BaseFragment;
import com.unitedwebspace.starlightbar.Bluetooth.BluetoothService;
import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.Common.Constants;
import com.unitedwebspace.starlightbar.Common.SignalModel;
import com.unitedwebspace.starlightbar.Main.MainActivity;
import com.unitedwebspace.starlightbar.R;

import java.util.Timer;
import java.util.TimerTask;

@SuppressLint("ValidFragment")
public class StreetModeFragment extends BaseFragment {

    MainActivity activity;
    BluetoothService mBluetoothService;
    BluetoothGattCharacteristic characteristic;

    TimerTask task;
    Timer timer;

    int selectedButtonId = 6, cnt = 0;;

    ImageView imvLeftBlinker, imvRightBlinker, imvCargo, imvAUX, imvHazard, imvLeftBlinkerBG, imvRightBlinkerBG, imvCargoBG, imvAUXBG, imvHazardBG;
    TextView  txvDeviceName;

    byte[] command = new byte[27];
    ImageView[] buttons = new ImageView[5];
    ImageView blinkingImageView;

    public StreetModeFragment(MainActivity activity) {

        this.activity = activity;
        mBluetoothService = activity.getBluetoothService();

        selectedButtonId = 6;

//        command = Commons.baseSignal;

        if (Commons.currentMode == Constants.E_STREETMODE){
            command = Commons.baseSignal;
        }else {
            SignalModel model = new SignalModel();
            command = model.initSendSignalArray();
            sendSignal();
        }

        blinkingView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_street_mode, container, false);

        imvLeftBlinkerBG = (ImageView)fragment.findViewById(R.id.imvLeftBlinkerBG);
        imvRightBlinkerBG = (ImageView)fragment.findViewById(R.id.imvRightBlinkerBG);
        imvCargoBG = (ImageView)fragment.findViewById(R.id.imvCargoBG);
        imvAUXBG = (ImageView)fragment.findViewById(R.id.imvAUXBG);
        imvHazardBG = (ImageView)fragment.findViewById(R.id.imvHazardBG);

        txvDeviceName = (TextView)fragment.findViewById(R.id.txvDeviceName);
        txvDeviceName.setText(Commons.connectedDevice.getOrgName());

        imvLeftBlinker = (ImageView)fragment.findViewById(R.id.imvLeftBlinker);
        imvLeftBlinker.setOnClickListener(this);

        imvRightBlinker = (ImageView)fragment.findViewById(R.id.imvRightBlinker);
        imvRightBlinker.setOnClickListener(this);

        imvCargo = (ImageView)fragment.findViewById(R.id.imvCargo);
        imvCargo.setOnClickListener(this);

        imvAUX = (ImageView)fragment.findViewById(R.id.imvAUX);
        imvAUX.setOnClickListener(this);

        imvHazard = (ImageView)fragment.findViewById(R.id.imvHazard);
        imvHazard.setOnClickListener(this);

        buttons[0] = imvLeftBlinkerBG; buttons[1] = imvHazardBG; buttons[2] = imvRightBlinkerBG; buttons[3] = imvCargoBG; buttons[4] = imvAUXBG;

        syncWithDeviceSetting();

        return fragment;
    }

    private void syncWithDeviceSetting() {

        for (int i = 0; i < buttons.length; i++){
            buttons[i].setVisibility(View.VISIBLE);
        }

        byte currentSetting = command[2];
        switch (currentSetting){
            case 1:
                buttons[0].setAlpha(0.5f);
                selectedButtonId = 0;
                break;
            case 2:
                buttons[2].setAlpha(0.5f);
                selectedButtonId = 2;
                break;
            case 4:
                buttons[3].setAlpha(0.5f);
                selectedButtonId = 3;
                break;
            case 8:
                buttons[1].setAlpha(0.5f);
                selectedButtonId = 1;
                break;
            case 10:
                buttons[4].setAlpha(0.5f);
                selectedButtonId = 4;
                break;
            case 12:
                selectedButtonId = 5;
                break;
        }
        initBtnBG();
    }

    @SuppressLint("NewApi")
    private void sendSignal() {

        characteristic = mBluetoothService.getCharacteristic();

        Commons.currentMode = Constants.E_STREETMODE;

        Commons.baseSignal = command;

        mBluetoothService.write(characteristic.getService().getUuid().toString(),
                characteristic.getUuid().toString(), command,
                new BleCharacterCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {
                    }

                    @Override
                    public void onFailure(BleException exception) {}

                    @Override
                    public void onInitiatedResult(boolean result) {
                    }
                });
    }

    private void makeCommand (int signalId){

        byte value = 0;
        switch (signalId){
            case 0:
                value = SignalModel.BT_STREET_MODE.MMLEFTTURN.asByte();
                command[1] = SignalModel.BT_MODE.e_STREETMODE.asByte();
                command[2] = value;
                break;
            case 1:
                value = SignalModel.BT_STREET_MODE.MMHAZARD.asByte();
                command[1] = SignalModel.BT_MODE.e_STREETMODE.asByte();
                command[2] = value;
                break;
            case 2:
                value = SignalModel.BT_STREET_MODE.MMRIGHTTURN.asByte();
                command[1] = SignalModel.BT_MODE.e_STREETMODE.asByte();
                command[2] = value;
                break;
            case 3:
                value = SignalModel.BT_STREET_MODE.MMCARGO.asByte();
                command[1] = SignalModel.BT_MODE.e_STREETMODE.asByte();
                command[2] = value;
                break;
            case 4:
                value = SignalModel.BT_STREET_MODE.MMAUXILIARY.asByte();
                command[1] = SignalModel.BT_MODE.e_STREETMODE.asByte();
                command[2] = value;
                break;
            case 5:
                value = SignalModel.BT_STREET_MODE.MMHAZARD_CARGO.asByte();
                command[1] = SignalModel.BT_MODE.e_STREETMODE.asByte();
                command[2] = value;
                break;
            case 10:
                command[1] = SignalModel.BT_MODE.e_STREETMODE.asByte();
                command[2] = value;
                break;
        }
    }

    private void initBtnBG(){
        makeCommand(10);
        for (int i = 0; i < buttons.length; i++){
            buttons[i].setVisibility(View.INVISIBLE);
        }

        blinkingImageView = null;

        if (selectedButtonId < 5 && selectedButtonId > -1){
            buttons[selectedButtonId].setVisibility(View.VISIBLE);
            if (selectedButtonId != 3){
                blinkingImageView = buttons[selectedButtonId];
            }
            makeCommand(selectedButtonId);
        }
        if (selectedButtonId == 5){
            buttons[1].setVisibility(View.VISIBLE);
            blinkingImageView = buttons[1];
            buttons[3].setVisibility(View.VISIBLE);
            makeCommand(selectedButtonId);
        }
    }

    private void change(int index) {
        selectedButtonId = index;
        initBtnBG();
        sendSignal();
    }

    ///// Blinking One View
    private void blinkingView(){
        task = new TimerTask() {
            @Override
            public void run() {
                callDefaultThreadForOneView();
            }
        };
        timer = new Timer();
        timer.scheduleAtFixedRate(task, 0, 500);
    }

    void callDefaultThreadForOneView() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                updateUIForOneView();
            }
        }).start();
    }

    void updateUIForOneView() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (blinkingImageView == null) return;

                cnt ++;
                if (cnt % 2 == 0){
                    blinkingImageView.setVisibility(View.INVISIBLE);
                }else {
                    blinkingImageView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        blinkingImageView = null;
        task.cancel();
        timer.cancel();
        task = null;
        timer = null;
        buttons = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvLeftBlinker:
                if (selectedButtonId == 6) {
                    change(0);
                    break;
                }
                if (selectedButtonId == 0) {
                    change(6);
                    break;
                }
                change(0);
                break;
            case R.id.imvHazard:
                if (selectedButtonId == 6) {
                    change(1);
                    break;
                }
                if (selectedButtonId == 1) {
                    change(6);
                    break;
                }
                if (selectedButtonId == 3) {
                    change(5);
                    break;
                }
                if (selectedButtonId == 5) {
                    change(3);
                    break;
                }
                change(1);
                break;
            case R.id.imvRightBlinker:
                if (selectedButtonId == 6) {
                    change(2);
                    break;
                }
                if (selectedButtonId == 2) {
                    change(6);
                    break;
                }
                change(2);
                break;
            case R.id.imvCargo:
                if (selectedButtonId == 6) {
                    change(3);
                    break;
                }
                if (selectedButtonId == 3) {
                    change(6);
                    break;
                }
                if (selectedButtonId == 1) {
                    change(5);
                    break;
                }
                if (selectedButtonId == 5) {
                    change(1);
                    break;
                }
                change(3);
                break;
            case R.id.imvAUX:
                if (selectedButtonId == 6) {
                    change(4);
                    break;
                }
                if (selectedButtonId == 4) {
                    change(6);
                    break;
                }
                change(4);
                break;
        }
    }
}
