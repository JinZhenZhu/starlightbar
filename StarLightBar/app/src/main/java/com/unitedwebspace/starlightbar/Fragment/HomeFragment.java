package com.unitedwebspace.starlightbar.Fragment;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clj.fastble.conn.BleCharacterCallback;
import com.clj.fastble.exception.BleException;
import com.unitedwebspace.starlightbar.Base.BaseFragment;
import com.unitedwebspace.starlightbar.Bluetooth.BluetoothService;
import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.Common.Constants;
import com.unitedwebspace.starlightbar.Common.SignalModel;
import com.unitedwebspace.starlightbar.Main.MainActivity;
import com.unitedwebspace.starlightbar.R;

@SuppressLint("ValidFragment")
public class HomeFragment extends BaseFragment {

    MainActivity activity;
    BluetoothService mBluetoothService;
    BluetoothGattCharacteristic characteristic;

    TextView txvZoneA, txvZoneB, txvZoneC, txvZone1, txvZone2, txvZone3, txvZone4, txvZone5, txvZone6, txvDeviceName;
    ImageView imvDevicePic;

    byte[] command = new byte[27];

    public HomeFragment(MainActivity activity) {
        this.activity = activity;

        mBluetoothService = activity.getBluetoothService();

//        command = Commons.baseSignal;

        if (Commons.currentMode == Constants.E_DRIVINGMODE){
            command = Commons.baseSignal;
        }else {
            SignalModel model = new SignalModel();
            command = model.initSendSignalArray();
            sendDataToBT();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_home, container, false);

        imvDevicePic = (ImageView)fragment.findViewById(R.id.imvDevicePic);

        txvDeviceName = (TextView)fragment.findViewById(R.id.txvDeviceName);
        txvDeviceName.setText(Commons.connectedDevice.getOrgName());

        txvZoneA = (TextView)fragment.findViewById(R.id.txvZoneA);
        txvZoneA.setOnClickListener(this);

        txvZoneB = (TextView)fragment.findViewById(R.id.txvZoneB);
        txvZoneB.setOnClickListener(this);

        txvZoneC = (TextView)fragment.findViewById(R.id.txvZoneC);
        txvZoneC.setOnClickListener(this);

        txvZone1 = (TextView)fragment.findViewById(R.id.txvZone1);
        txvZone1.setOnClickListener(this);

        txvZone2 = (TextView)fragment.findViewById(R.id.txvZone2);
        txvZone2.setOnClickListener(this);

        txvZone3 = (TextView)fragment.findViewById(R.id.txvZone3);
        txvZone3.setOnClickListener(this);

        txvZone4 = (TextView)fragment.findViewById(R.id.txvZone4);
        txvZone4.setOnClickListener(this);

        txvZone5 = (TextView)fragment.findViewById(R.id.txvZone5);
        txvZone5.setOnClickListener(this);

        txvZone6 = (TextView)fragment.findViewById(R.id.txvZone6);
        txvZone6.setOnClickListener(this);

        imvDevicePic.setImageResource(getDevicePic(Commons.connectedDevice.getSettingInfo()[2]));

        return fragment;
    }

    private int getDevicePic(byte whoAmI) {

        int devicePic = R.drawable.ic_bluetooth;

        switch (whoAmI){
            case 0:
                devicePic = R.drawable.slb_m1;
                break;
            case 1:
                devicePic = R.drawable.slb_m2;
                break;
            case 2:
                devicePic = R.drawable.slb_m3;
                break;
            case 3:
                devicePic = R.drawable.slb_m4;
                break;
            case 4:
                devicePic = R.drawable.slb_m5;
                break;
            case 5:
                devicePic = R.drawable.slb_gx_f;
                break;
            case 6:
                devicePic = R.drawable.slb_gx_r;
                break;
            case 7:
                devicePic = R.drawable.slb_gx_w;
                break;
            case 8:
                devicePic = R.drawable.slb_gx_wr;
                break;
        }

        return devicePic;
    }

    @SuppressLint("NewApi")
    private void sendDataToBT() {

        Commons.currentMode = Constants.E_DRIVINGMODE;
        Commons.baseSignal = command;

        characteristic = mBluetoothService.getCharacteristic();
        mBluetoothService.write(characteristic.getService().getUuid().toString(),
                characteristic.getUuid().toString(), command,
                new BleCharacterCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {}

                    @Override
                    public void onFailure(BleException exception) {
                        showToast("Failure");
                    }

                    @Override
                    public void onInitiatedResult(boolean result) {}
                });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.txvZoneA:
                Commons.selectedZone = "Zone A";
                activity.showZoneFragment(0);
                break;
            case R.id.txvZoneB:
                Commons.selectedZone = "Zone B";
                activity.showZoneFragment(1);
                break;
            case R.id.txvZoneC:
                Commons.selectedZone = "Zone C";
                activity.showZoneFragment(2);
                break;
            case R.id.txvZone1:
                Commons.selectedZone = "Zone 1";
                activity.showZoneFragment(7);
                break;
            case R.id.txvZone2:
                Commons.selectedZone = "Zone 2";
                activity.showZoneFragment(8);
                break;
            case R.id.txvZone3:
                Commons.selectedZone = "Zone 3";
                activity.showZoneFragment(9);
                break;
            case R.id.txvZone4:
                Commons.selectedZone = "Zone 4";
                activity.showZoneFragment(10);
                break;
            case R.id.txvZone5:
                Commons.selectedZone = "Zone 5";
                activity.showZoneFragment(11);
                break;
            case R.id.txvZone6:
                Commons.selectedZone = "Zone 6";
                activity.showZoneFragment(12);
                break;
        }
    }
}
