package com.unitedwebspace.starlightbar.Main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.clj.fastble.conn.BleCharacterCallback;
import com.clj.fastble.data.ScanResult;
import com.clj.fastble.exception.BleException;
import com.unitedwebspace.starlightbar.Adapter.BTModel;

import com.unitedwebspace.starlightbar.Adapter.ResultAdapter;
import com.unitedwebspace.starlightbar.Base.BaseActivity;
import com.unitedwebspace.starlightbar.Bluetooth.BluetoothService;
import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.Common.Constants;
import com.unitedwebspace.starlightbar.R;
import com.unitedwebspace.starlightbar.SQLite.BTDBModel;
import com.unitedwebspace.starlightbar.SQLite.BTDeviceLocalStore;

import java.util.ArrayList;
import java.util.List;

public class StartupActivity extends BaseActivity{

    BluetoothService mBluetoothService;
    BluetoothGattCharacteristic characteristic;
    ResultAdapter mResultAdapter;
    ArrayList<BTModel> mBTModelList = new ArrayList<>();

    Dialog dialog;
    Button btnFindDevice;
    ListView lstDeviceList;
    ImageView imvLogo, imvBG;

    boolean isSelectedBLE = false;
    int index = 0;
    byte[] command = new byte[27];

    boolean isNewDevice = true;
    boolean sendPassword = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        loadLayout();
    }

    private void loadLayout() {

        imvBG = (ImageView)findViewById(R.id.imvBG);

        imvLogo = (ImageView)findViewById(R.id.imvLogo);

        btnFindDevice = (Button)findViewById(R.id.btnFindDevice);
        btnFindDevice.setOnClickListener(this);

        this.mResultAdapter =  new ResultAdapter(this);
        lstDeviceList = (ListView)findViewById(R.id.lstDeviceList);
        lstDeviceList.setAdapter(mResultAdapter);
        lstDeviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mBluetoothService != null) {
                    isSelectedBLE = true;
                    Commons.connectedDevice = mBTModelList.get(position);
                    mBluetoothService.cancelScan();
                    mBluetoothService.connectDevice(mBTModelList.get(position).getResult());
                }
            }
        });
    }

    private void checkPermissions() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
        List<String> permissionDeniedList = new ArrayList<>();
        for (String permission : permissions) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, permission);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                onPermissionGranted(permission);
            } else {
                permissionDeniedList.add(permission);
            }
        }
        if (!permissionDeniedList.isEmpty()) {
            String[] deniedPermissions = permissionDeniedList.toArray(new String[permissionDeniedList.size()]);
            ActivityCompat.requestPermissions(this, deniedPermissions, 12);
        }
    }

    private void onPermissionGranted(String permission) {
        switch (permission) {
            case Manifest.permission.ACCESS_FINE_LOCATION:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !checkGPSIsOpen()) {
                    new AlertDialog.Builder(this).setTitle(R.string.notifyTitle).setMessage(R.string.gpsNotifyMsg).setNegativeButton(R.string.cancel,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    }).setPositiveButton(R.string.setting,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                            startActivityForResult(intent, 1);
                                        }
                                    }).setCancelable(false).show();
                } else {
                    startScan();
                }
                break;
        }
    }

    private boolean checkGPSIsOpen() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager == null)
            return false;
        return locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
    }

    private void startScan() {
        if (mBluetoothService == null) {
            bindService();
        } else {
            mBluetoothService.scanDevice();
        }
    }

    private void bindService() {
        Intent bindIntent = new Intent(this, BluetoothService.class);
        this.bindService(bindIntent, mFhrSCon, Context.BIND_AUTO_CREATE);
    }

    private void unbindService() {
        this.unbindService(mFhrSCon);
    }

    private ServiceConnection mFhrSCon = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBluetoothService = ((BluetoothService.BluetoothBinder) service).getService();
            mBluetoothService.setScanCallback(callback);
            mBluetoothService.scanDevice();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBluetoothService = null;
        }
    };

    private BluetoothService.Callback callback = new BluetoothService.Callback() {
        @Override
        public void onStartScan() {
            showProgress();
            mBTModelList.clear();
            index = 0;
            btnFindDevice.setVisibility(View.VISIBLE);
            lstDeviceList.setVisibility(View.INVISIBLE);
            imvBG.setVisibility(View.VISIBLE);
        }

        @Override
        public void onScanning(ScanResult result) {
            BTModel model = new BTModel();
            model.setOrgName(result.getDevice().getName());
            model.setResult(result);

            if (availableBLE(model.getOrgName())) mBTModelList.add(model);
        }

        @Override
        public void onScanComplete() {
            closeProgress();
            getSettingInfo();
        }

        @Override
        public void onConnecting() {
            showProgress();
        }

        @Override
        public void onConnectFail() {
            closeProgress();
            btnFindDevice.setVisibility(View.VISIBLE);
            lstDeviceList.setVisibility(View.INVISIBLE);
            imvBG.setVisibility(View.VISIBLE);
            closeProgress();
            showToast("Connection failed");

            if (Commons.mMainActivity != null) Commons.mMainActivity.finish();
        }

        @Override
        public void onDisConnected() {
            closeProgress();
            mBTModelList.clear();
            mResultAdapter.refresh(mBTModelList);

            Log.d("disconnected", "device is disconnected");
            btnFindDevice.setVisibility(View.VISIBLE);
            lstDeviceList.setVisibility(View.INVISIBLE);
            imvBG.setVisibility(View.VISIBLE);
            isSelectedBLE = false;
            isNewDevice = false;
            index = 0;

            if (Commons.mMainActivity != null) Commons.mMainActivity.finish();
            if (sendPassword) {
                Commons.mBTBDModel.delete();
                sendPassword = false;
                Commons.btDeviceList.clear();
                BTDeviceLocalStore btDeviceLocalStore = new BTDeviceLocalStore();
                Commons.btDeviceList = btDeviceLocalStore.loadBTDevices();
            }
        }

        @Override
        public void onServicesDiscovered() {
            if (isSelectedBLE){
                configBLE();
                confirmPassword(Commons.connectedDevice);
            }else {
                configBLE();
                getWhoAmIBit();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        mBluetoothService.closeConnect();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                if (index < mBTModelList.size() - 1){
                                    index++;
                                    getSettingInfo();
                                }else {
                                    index = 0;
                                    closeProgress();
                                    updateDataAsync();
                                }
                            }
                        }, 1000);
                    }
                }, 1000);
            }
        }
    };

    private void confirmPassword(BTModel connectedDevice) {
        BTDeviceLocalStore btDeviceLocalStore = new BTDeviceLocalStore();
        Commons.btDeviceList = btDeviceLocalStore.loadBTDevices();

        isNewDevice = true;
        for (int i = 0; i < Commons.btDeviceList.size(); i ++){
            if (Commons.btDeviceList.get(i).getOrgName().equals(connectedDevice.getOrgName())){
                isNewDevice = false;
                Commons.connectedDevice.setOrgPwd(Commons.btDeviceList.get(i).getOrgPwd());
                Commons.mBTBDModel = Commons.btDeviceList.get(i);
            }
        }

        if (isNewDevice) {
            inputPassword();
        }else {
            sendPassword(Commons.connectedDevice.getOrgPwd());
        }
    }

    private void inputPassword() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_set_password);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final EditText edtPassword = (EditText)dialog.findViewById(R.id.edtPassword);

        Button btnConfirm = (Button)dialog.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = edtPassword.getText().toString().trim();
                sendPassword(password);
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private boolean availableBLE(String deviceName) {
        if (deviceName ==  null) return false;
        if (deviceName.isEmpty() || !deviceName.contains(Constants.CONTAINED_DEVICE_NAME)){return  false;}
        return true;
    }

    private void getSettingInfo() {
        mBluetoothService.cancelScan();
        if (mBTModelList.size() == 0) {
            showBottomToast("No Device");
//            refreshList();
        }else {
            mBluetoothService.connectDevice(mBTModelList.get(index).getResult());
        }
    }

    @SuppressLint("NewApi")
    private void configBLE() {

        BluetoothGatt gatt = mBluetoothService.getGatt();

        for (final BluetoothGattService service : gatt.getServices()){

            for (final BluetoothGattCharacteristic characteristic : service.getCharacteristics()){
                if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0){
                    mBluetoothService.setService(service);
                    mBluetoothService.setCharacteristic(characteristic);
                }
            }
        }
    }

    @SuppressLint("NewApi")
    private void getWhoAmIBit() {
        command = makeByteData("07:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00");

        characteristic = mBluetoothService.getCharacteristic();

        mBluetoothService.write(characteristic.getService().getUuid().toString(),
                characteristic.getUuid().toString(), command,
                new BleCharacterCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {}
                    @Override
                    public void onFailure(BleException exception) {}
                    @Override
                    public void onInitiatedResult(boolean result) {}
                });

        mBluetoothService.read(characteristic.getService().getUuid().toString(), characteristic.getUuid().toString(), new BleCharacterCallback() {
            @Override
            public void onFailure(BleException exception) {}

            @Override
            public void onInitiatedResult(boolean result) {}

            @Override
            public void onSuccess(BluetoothGattCharacteristic characteristic) {
                mBTModelList.get(index).setSettingInfo(characteristic.getValue());
            }
        });
    }

    @SuppressLint("NewApi")
    private void sendPassword(final String password){
        command = makePwdCommand(password);

        sendPassword = true;

        characteristic = mBluetoothService.getCharacteristic();

        mBluetoothService.write(characteristic.getService().getUuid().toString(),
                characteristic.getUuid().toString(), command,
                new BleCharacterCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {}
                    @Override
                    public void onFailure(BleException exception) {}
                    @Override
                    public void onInitiatedResult(boolean result) {}
                });

        mBluetoothService.read(characteristic.getService().getUuid().toString(), characteristic.getUuid().toString(), new BleCharacterCallback() {
            @Override
            public void onFailure(BleException exception) {}
            @Override
            public void onInitiatedResult(boolean result) {}
            @Override
            public void onSuccess(BluetoothGattCharacteristic characteristic) {
                if (characteristic.getValue()[
                        0] == 7){
                    if (isNewDevice){
                        saveToDB(Commons.connectedDevice, password);
                        isNewDevice = false;
                    }else {
                        updateDB(Commons.connectedDevice.getOrgPwd());
                    }
                    closeProgress();
                    Commons.mBluetoothService = mBluetoothService;
                    startActivity(new Intent(StartupActivity.this, MainActivity.class));
                }else {
                    inputPassword();
                }
            }
        });
    }

    private void updateDB(String password) {
        BTDeviceLocalStore btDeviceLocalStore =  new BTDeviceLocalStore();
        Commons.mBTBDModel.setOrgPwd(password);
        btDeviceLocalStore.saveBTDevice(Commons.mBTBDModel);
    }

    private void saveToDB(BTModel model, String password) {
        BTDBModel mBTDBModel = new BTDBModel();
        mBTDBModel.setOrgName(model.getOrgName());
        mBTDBModel.setOrgPwd(password);

        BTDeviceLocalStore deviceLocalStore =  new BTDeviceLocalStore();
        deviceLocalStore.saveBTDevice(mBTDBModel);
    }

    private void updateDataAsync() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                refreshList();
            }
        }).start();
    }

    private void refreshList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnFindDevice.setVisibility(View.GONE);
                lstDeviceList.setVisibility(View.VISIBLE);
                imvBG.setVisibility(View.INVISIBLE);
                mResultAdapter.refresh(mBTModelList);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBluetoothService != null)
            unbindService();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnFindDevice:

                checkPermissions();
                break;
        }
    }
}
