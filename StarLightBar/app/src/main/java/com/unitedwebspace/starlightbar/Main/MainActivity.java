package com.unitedwebspace.starlightbar.Main;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.clj.fastble.conn.BleCharacterCallback;
import com.clj.fastble.exception.BleException;
import com.unitedwebspace.starlightbar.Base.BaseActivity;
import com.unitedwebspace.starlightbar.Bluetooth.BluetoothService;
import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.Common.Constants;
import com.unitedwebspace.starlightbar.Common.SignalParser;
import com.unitedwebspace.starlightbar.Fragment.BrightnessFragment;
import com.unitedwebspace.starlightbar.Fragment.DeviceInfoFragment;
import com.unitedwebspace.starlightbar.Fragment.HomeFragment;
import com.unitedwebspace.starlightbar.Fragment.ModeFragment;
import com.unitedwebspace.starlightbar.Fragment.StreetModeFragment;
import com.unitedwebspace.starlightbar.Fragment.ZoneFragment;
import com.unitedwebspace.starlightbar.R;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity {

    BluetoothGattCharacteristic characteristic;
    BluetoothService mBluetoothService;

    DeviceInfoFragment mDeviceNameFragment;
    HomeFragment mHomeFragment;
    ZoneFragment mZoneFragment;
    BrightnessFragment mBrightnessFragment;
    ModeFragment mModeFragment;
    StreetModeFragment mStreetModeFragment;

    LinearLayout lytDeviceInfo, lytHome, lytBrightness, lytMode;
    SignalParser parser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Commons.isLogout = false;

        mBluetoothService = Commons.mBluetoothService;

        getDeviceSetting();

        loadLayout();

        Commons.mMainActivity = this;
    }

    private void loadLayout() {

        lytHome = (LinearLayout) findViewById(R.id.lytHome);
        lytHome.setOnClickListener(this);

        lytDeviceInfo = (LinearLayout) findViewById(R.id.lytDeviceInfo);
        lytDeviceInfo.setOnClickListener(this);

        lytBrightness = (LinearLayout) findViewById(R.id.lytBrightness);
        lytBrightness.setOnClickListener(this);

        lytMode = (LinearLayout) findViewById(R.id.lytMode);
        lytMode.setOnClickListener(this);

        showDeviceInfoFragment();

    }

    private void initFooterTab() {
        lytDeviceInfo.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        lytHome.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        lytBrightness.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        lytMode.setBackgroundColor(getResources().getColor(R.color.colorWhite));
    }

    private void showDeviceInfoFragment() {
        initFooterTab();
        lytDeviceInfo.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mDeviceNameFragment = new DeviceInfoFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mDeviceNameFragment);
        fragmentTransaction.commit();
    }

    public void showHomeFragment() {

        initFooterTab();
        lytHome.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));

        if (Commons.currentMode ==  Constants.E_STREETMODE){
            showStreetModeFragment();
        }else if (Commons.currentMode ==  Constants.E_DRIVINGMODE){
            FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
            mHomeFragment = new HomeFragment(this);
            fragmentTransaction.replace(R.id.fragContainer, mHomeFragment);
            fragmentTransaction.commit();
        }
    }

    public void showHomeFragment1() {
        initFooterTab();
        lytHome.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mHomeFragment = new HomeFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mHomeFragment);
        fragmentTransaction.commit();
    }

    public void showZoneFragment(int index){
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mZoneFragment = new ZoneFragment(this, index);
        fragmentTransaction.replace(R.id.fragContainer, mZoneFragment);
        fragmentTransaction.commit();
    }

    private void showBrightnessFragment() {
        initFooterTab();
        lytBrightness.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mBrightnessFragment = new BrightnessFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mBrightnessFragment);
        fragmentTransaction.commit();
    }

    private void showModeFragment() {
        initFooterTab();
        lytMode.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mModeFragment = new ModeFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mModeFragment);
        fragmentTransaction.commit();
    }

    public void showStreetModeFragment(){

        initFooterTab();
        lytHome.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mStreetModeFragment = new StreetModeFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mStreetModeFragment);
        fragmentTransaction.commit();
    }

    public BluetoothService getBluetoothService(){
        return Commons.mBluetoothService;
    }

    @SuppressLint("NewApi")
    public void getDeviceSetting(){

        characteristic = mBluetoothService.getCharacteristic();

        byte[] command = makeByteData("07:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00");

        mBluetoothService.write(characteristic.getService().getUuid().toString(),
                characteristic.getUuid().toString(), command,
                new BleCharacterCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {}

                    @Override
                    public void onFailure(BleException exception) {}

                    @Override
                    public void onInitiatedResult(boolean result) {}
                });

        mBluetoothService.read(characteristic.getService().getUuid().toString(), characteristic.getUuid().toString(), new BleCharacterCallback() {
            @Override
            public void onFailure(BleException exception) {}

            @Override
            public void onInitiatedResult(boolean result) {}

            @Override
            public void onSuccess(BluetoothGattCharacteristic characteristic) {
                Commons.connectedDevice.setSettingInfo(characteristic.getValue());
                Commons.currentMode = Commons.connectedDevice.getSettingInfo()[3];
                parser = new SignalParser();
                parser.getDeviceState();
            }
        });
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        Log.d("aaaaa", "Finish");
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        Commons.mMainActivity = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    @SuppressLint("NewApi")
    public void setResetCommand() {
        characteristic = mBluetoothService.getCharacteristic();

        byte[] command = makeByteData("06:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00");

        mBluetoothService.write(characteristic.getService().getUuid().toString(),
                characteristic.getUuid().toString(), command,
                new BleCharacterCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {}

                    @Override
                    public void onFailure(BleException exception) {}

                    @Override
                    public void onInitiatedResult(boolean result) {}
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lytDeviceInfo:
                showDeviceInfoFragment();
                break;
            case R.id.lytHome:
                if (Commons.currentMode == Constants.E_OEMMODE || Commons.currentMode == Constants.E_OFF) return;
                showHomeFragment();
                break;
            case R.id.lytBrightness:
                showBrightnessFragment();
                break;
            case R.id.lytMode:
                showModeFragment();
                break;
        }
    }
}
