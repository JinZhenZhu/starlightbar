package com.unitedwebspace.starlightbar.Common;

import com.unitedwebspace.starlightbar.Adapter.BTModel;
import com.unitedwebspace.starlightbar.Bluetooth.BluetoothService;
import com.unitedwebspace.starlightbar.Main.MainActivity;
import com.unitedwebspace.starlightbar.SQLite.BTDBModel;

import java.util.ArrayList;
import java.util.List;

public class Commons {
    public static List<BTDBModel> btDeviceList = new ArrayList<>();
    public static BTModel connectedDevice;
    public static String selectedZone = "";
    public static byte[] baseSignal = new byte[27];
    public static byte currentBrightness = 0;
    public static BTDBModel mBTBDModel;
    public static byte currentMode = 0;
    public static BluetoothService mBluetoothService;
    public static boolean  isLogout = false;
    public static MainActivity mMainActivity ;
}
