package com.unitedwebspace.starlightbar.SQLite;

import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

public class BTDeviceLocalStore implements BTDeviceStore {

    @Override
    public List<BTDBModel> loadBTDevices() {
        List<BTDBModel> btDeviceList = SQLite.select().from(BTDBModel.class).queryList();
        return btDeviceList;
    }

    @Override
    public void saveBTDeviceList(List<BTDBModel> btDeviceList) {
        for (BTDBModel btDevice :btDeviceList) {
            btDevice.save();
        }
    }

    @Override
    public BTDBModel loadBTDevice(int btDeviceId) {
        BTDBModel btDevice = SQLite.select()
                            .from(BTDBModel.class)
                            .where(BTDBModel_Table.id.is(btDeviceId))
                            .querySingle();
        return btDevice;
    }

    @Override
    public BTDBModel loadBTDevice(String orgName) {
        BTDBModel btDevice = SQLite.select()
                            .from(BTDBModel.class)
                            .where(BTDBModel_Table.orgName.is(orgName))
                            .querySingle();
        return btDevice;
    }

    @Override
    public void saveBTDevice(BTDBModel btDevice) {
        btDevice.save();
    }

    @Override
    public void deleteBTDevice(BTDBModel btDevice) {
        btDevice.delete();
    }



    @Override
    public void clean() {
        Delete.table(BTDBModel.class);
    }
}
