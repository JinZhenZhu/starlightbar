package com.unitedwebspace.starlightbar.Main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.unitedwebspace.starlightbar.Base.BaseActivity;
import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.R;
import com.unitedwebspace.starlightbar.SQLite.BTDeviceLocalStore;
import com.unitedwebspace.starlightbar.Service.ResetService;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getAllSavedBT();

        gotoStartupActivity();
        runService();
    }

    private void runService(){
        Intent serviceIntent = new Intent(this, ResetService.class);
        startService(serviceIntent);
    }

    private void getAllSavedBT() {
        BTDeviceLocalStore btDeviceLocalStore = new BTDeviceLocalStore();
        Commons.btDeviceList = btDeviceLocalStore.loadBTDevices();
        Log.d("aaaa", "aaaa");
    }

    private void gotoStartupActivity() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, StartupActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1000);
    }
}
