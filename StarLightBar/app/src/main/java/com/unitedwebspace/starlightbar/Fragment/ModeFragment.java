package com.unitedwebspace.starlightbar.Fragment;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clj.fastble.conn.BleCharacterCallback;
import com.clj.fastble.exception.BleException;
import com.unitedwebspace.starlightbar.Base.BaseFragment;
import com.unitedwebspace.starlightbar.Bluetooth.BluetoothService;
import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.Common.Constants;
import com.unitedwebspace.starlightbar.Common.SignalModel;
import com.unitedwebspace.starlightbar.Main.MainActivity;
import com.unitedwebspace.starlightbar.R;

@SuppressLint("ValidFragment")
public class ModeFragment extends BaseFragment {

    MainActivity activity;
    BluetoothService mBluetoothService;
    BluetoothGattCharacteristic characteristic;

    RadioButton rbStreet,rbOffRoad, rbOem, rbLightBarOff;
    ImageView imvLogo;
    TextView txvDeviceName;

    int index = 2;
    byte value = 0;
    byte[] command = new byte[27];

    public ModeFragment(MainActivity activity) {
        this.activity = activity;
        command = Commons.baseSignal;
        mBluetoothService = activity.getBluetoothService();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_mode, container, false);

        imvLogo = (ImageView)fragment.findViewById(R.id.imvLogo);

        rbStreet = (RadioButton)fragment.findViewById(R.id.rbStreet);
        rbStreet.setOnClickListener(this);

        rbOffRoad = (RadioButton)fragment.findViewById(R.id.rbOffRoad);
        rbOffRoad.setOnClickListener(this);

        rbOem = (RadioButton)fragment.findViewById(R.id.rbOem);
        rbOem.setOnClickListener(this);

        rbLightBarOff = (RadioButton)fragment.findViewById(R.id.rbLightBarOff);
        rbLightBarOff.setOnClickListener(this);

        txvDeviceName = (TextView)fragment.findViewById(R.id.txvDeviceName);
        txvDeviceName.setText(Commons.connectedDevice.getOrgName());

        value = SignalModel.BT_MODE.e_STREETMODE.asByte();

        initSetMode();

        return fragment;
    }

    private void initSetMode() {

        /*byte selectedMode = command[1];
        Commons.currentMode = selectedMode;*/

        byte selectedMode =  Commons.currentMode;

        switch (selectedMode){
            case Constants.E_STREETMODE:
                rbStreet.setChecked(true);
                value = SignalModel.BT_MODE.e_STREETMODE.asByte();
                break;
            case Constants.E_DRIVINGMODE:
                rbOffRoad.setChecked(true);
                value = SignalModel.BT_MODE.e_DRIVINGMODE.asByte();
                break;
            case Constants.E_OEMMODE:
                rbOem.setChecked(true);
                value = SignalModel.BT_MODE.e_OEMMODE.asByte();
                break;
            case Constants.E_OFF:
                rbLightBarOff.setChecked(true);
                value = SignalModel.BT_MODE.e_STREETMODE.asByte();
                break;
        }
    }

    @SuppressLint("NewApi")
    private void sendCommand (){
        characteristic = mBluetoothService.getCharacteristic();

        Commons.baseSignal = command;

        mBluetoothService.write(characteristic.getService().getUuid().toString(),
                characteristic.getUuid().toString(), command,
                new BleCharacterCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {
                    }

                    @Override
                    public void onFailure(BleException exception) {}

                    @Override
                    public void onInitiatedResult(boolean result) {
                    }
                });
    }

    private void initializeRadioButtons(){
        rbStreet.setChecked(false);
        rbOffRoad.setChecked(false);
        rbLightBarOff.setChecked(false);
        rbOem.setChecked(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rbStreet:
                initializeRadioButtons();
                rbStreet.setChecked(true);
                activity.showStreetModeFragment();
                break;
            case R.id.rbOffRoad:
                initializeRadioButtons();
                rbOffRoad.setChecked(true);
                activity.showHomeFragment1();
                break;
            case R.id.rbOem:
                initializeRadioButtons();
                rbOem.setChecked(true);
                SignalModel newSignal_1 = new SignalModel();
                command = newSignal_1.initSendSignalArray();
                command[1] = SignalModel.BT_MODE.e_OEMMODE.asByte();
                Commons.currentMode =  Constants.E_OEMMODE;
                sendCommand();
                break;
            case R.id.rbLightBarOff:
                initializeRadioButtons();
                rbLightBarOff.setChecked(true);
                SignalModel newSignal_2 = new SignalModel();
                command = newSignal_2.initSendSignalArray();
                command[1] = SignalModel.BT_MODE.e_OFF.asByte();
                Commons.currentMode = Constants.E_OFF;
                sendCommand();
                break;
        }
    }
}
