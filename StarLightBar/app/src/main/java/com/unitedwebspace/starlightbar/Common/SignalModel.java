package com.unitedwebspace.starlightbar.Common;

public class SignalModel {
    public enum BT_SPECIFIC_GATTS{
        BLE_IDLE_STATE((byte)0),
        GATTS_CMD_CHANGE_PWD((byte)1),
        GATTS_CMD_CHANGE_NAME((byte)2),
        GATTS_CMD_CONNECT_REQ((byte)3),
        GATTS_CMD_DISCONNECT((byte)4) ,
        GATTS_CMD_DATA((byte)5),
        GATTS_CMD_RESET((byte)6) ,
        GATTS_CMD_WHOIAM((byte)7);

        public byte asByte() {
            return asByte;
        }

        private final byte asByte;

        private BT_SPECIFIC_GATTS(byte asByte) {
            this.asByte = asByte;
        }
    }

    public enum BT_MODE{
        e_STREETMODE ((byte)0),
        e_DRIVINGMODE ((byte)1),
        e_OEMMODE ((byte)2),
        e_OFF ((byte)3);

        public byte asByte() {
            return asByte;
        }

        private final byte asByte;

        private BT_MODE(byte asByte) {
            this.asByte = asByte;
        }
    }

    public enum BT_STREET_MODE {
        MMLEFTTURN((byte)1),
        MMRIGHTTURN((byte)2),
        MMCARGO ((byte)4),
        MMHAZARD((byte)8),
        MMAUXILIARY ((byte)10),
        MMHAZARD_CARGO((byte)12);

        public byte asByte() {
            return asByte;
        }

        private final byte asByte;

        private BT_STREET_MODE(byte asByte) {
            this.asByte = asByte;
        }
    }

    public enum BT_ZONE_MODE{
        e_OFF ((byte) 0),     // off
        e_ON ((byte) 1),       // solid on

        e_FLASH1 ((byte) 2),   // flash with 5 rates
        e_FLASH2 ((byte) 3),
        e_FLASH3 ((byte) 4),
        e_FLASH4 ((byte) 5),
        e_FLASH5 ((byte) 6),

        e_FLASHALT1 ((byte) 7),    // alternate flash with 5 rates
        e_FLASHALT2 ((byte) 8),
        e_FLASHALT3 ((byte) 9),
        e_FLASHALT4 ((byte) 10),
        e_FLASHALT5 ((byte) 11),

        e_FLASHALTA1 ((byte) 12),   // alternate flash with 5 rates, the other state
        e_FLASHALTA2 ((byte) 13),
        e_FLASHALTA3 ((byte) 14),
        e_FLASHALTA4 ((byte) 15),
        e_FLASHALTA5 ((byte) 16),

        e_RANDOM1 ((byte) 17),      // slowest flashing, random pattern, 5 rates
        e_RANDOM2 ((byte) 18),
        e_RANDOM3 ((byte) 19),
        e_RANDOM4 ((byte) 20),
        e_RANDOM5 ((byte) 21);

        public byte asByte() {
            return asByte;
        }

        private final byte asByte;

        private BT_ZONE_MODE(byte asByte) {
            this.asByte = asByte;
        }
    }

    public enum BT_WhoIAm{
        e_SLB_M1 ((byte) 0),
        e_SLB_M2 ((byte) 1),
        e_SLB_M3 ((byte) 2),
        e_SLB_M4 ((byte) 3),
        e_SLB_M5 ((byte) 4),
        e_SLB_GXF ((byte) 5),
        e_SLB_GXR ((byte) 5),
        e_SLB_GXW ((byte) 7),
        e_SLB_GXWR ((byte) 8);

        public byte asByte() {
            return asByte;
        }

        private final byte asByte;

        private BT_WhoIAm(byte asByte) {
            this.asByte = asByte;
        }
    }

    public byte[] initSignalArray (){
        byte[] data = new byte[]{00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00};
        return data;
    }

    public byte[] addValueToSignalArray( byte[] rawValue, int index, byte value){

        byte[] result = rawValue;
        result[index -1] = value;
        return result;
    }

    public byte[] initSendSignalArray(){
        byte [] selectedSignal = initSignalArray();

        selectedSignal = addValueToSignalArray(selectedSignal, 1, BT_SPECIFIC_GATTS.GATTS_CMD_DATA.asByte());
        selectedSignal = addValueToSignalArray(selectedSignal, 13, Commons.currentBrightness);
        selectedSignal = addValueToSignalArray(selectedSignal, 14, Commons.currentBrightness);
        selectedSignal = addValueToSignalArray(selectedSignal, 15, Commons.currentBrightness);

        return selectedSignal;
    }
}
