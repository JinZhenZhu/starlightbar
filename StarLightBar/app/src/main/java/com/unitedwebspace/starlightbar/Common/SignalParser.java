package com.unitedwebspace.starlightbar.Common;

import java.io.Serializable;

public class SignalParser implements Serializable {

    byte[] baseSignal = new byte[]{05, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00};

    byte[] temp = Commons.connectedDevice.getSettingInfo();
    byte value = 0;

    public void getDeviceState(){

        //// get current brightness
        if (temp[11] == temp[12] && temp[12] == temp[13]){
            if (temp[11] < 25){
                Commons.currentBrightness = 100;
                baseSignal[12] = Commons.currentBrightness; baseSignal[13] = Commons.currentBrightness; baseSignal[14] = Commons.currentBrightness;
            }else {
                Commons.currentBrightness = temp[11];
                baseSignal[12] = Commons.currentBrightness; baseSignal[13] = Commons.currentBrightness; baseSignal[14] = Commons.currentBrightness;
            }
        }

        /// get current mode
        byte selectedMode = Commons.connectedDevice.getSettingInfo()[3];
        Commons.currentMode = selectedMode;

        switch (selectedMode){
            case Constants.E_STREETMODE:
                value = SignalModel.BT_MODE.e_STREETMODE.asByte();
                break;
            case Constants.E_DRIVINGMODE:
                value = SignalModel.BT_MODE.e_DRIVINGMODE.asByte();
                break;
            case Constants.E_OEMMODE:
                value = SignalModel.BT_MODE.e_OEMMODE.asByte();
                break;
            case Constants.E_OFF:
                value = SignalModel.BT_MODE.e_STREETMODE.asByte();
                break;
        }

        baseSignal[1] = value;

        if (selectedMode == Constants.E_STREETMODE){
            byte subModeInStreetMode = Commons.connectedDevice.getSettingInfo()[4];
            switch (subModeInStreetMode){
                case 1:
                    value = SignalModel.BT_STREET_MODE.MMLEFTTURN.asByte();
                    break;
                case 2:
                    value = SignalModel.BT_STREET_MODE.MMRIGHTTURN.asByte();
                    break;
                case 4:
                    value = SignalModel.BT_STREET_MODE.MMCARGO.asByte();
                    break;
                case 8:
                    value = SignalModel.BT_STREET_MODE.MMHAZARD.asByte();
                    break;
                case 10:
                    value = SignalModel.BT_STREET_MODE.MMAUXILIARY.asByte();
                    break;
                case 12:
                    value = SignalModel.BT_STREET_MODE.MMHAZARD_CARGO.asByte();
                    break;
            }

            baseSignal[2] =  value;

        }else if (selectedMode == Constants.E_DRIVINGMODE){
            byte[] isSelectedZoneA = new byte[]{Commons.connectedDevice.getSettingInfo()[5], Commons.connectedDevice.getSettingInfo()[6]};
            byte[] isSelectedZoneB = new byte[]{Commons.connectedDevice.getSettingInfo()[7], Commons.connectedDevice.getSettingInfo()[8]};
            byte[] isSelectedZoneC = new byte[]{Commons.connectedDevice.getSettingInfo()[9], Commons.connectedDevice.getSettingInfo()[10]};

            if (isSelectedZoneA[0] >0 && isSelectedZoneA[1] >0 && isSelectedZoneA[0] == isSelectedZoneA[1]){
                baseSignal[6] = isSelectedZoneA[0]; baseSignal[7] = isSelectedZoneA[0];
            }
            if (isSelectedZoneB[0] >0 && isSelectedZoneB[1] >0 && isSelectedZoneB[0] == isSelectedZoneB[1]){
                baseSignal[8] =  isSelectedZoneB[0]; baseSignal[9] =  isSelectedZoneB[0];
            }
            if (isSelectedZoneC[0] >0 && isSelectedZoneC[1] >0 && isSelectedZoneC[0] == isSelectedZoneC[1]){
                baseSignal[10] =  isSelectedZoneC[0]; baseSignal[11] =  isSelectedZoneB[0];
            }
            if (isSelectedZoneA[0] >0){
                baseSignal[6] =  isSelectedZoneA[0];
            }
            if (isSelectedZoneA[1] >0){
                baseSignal[7] =  isSelectedZoneA[1];
            }
            if (isSelectedZoneB[0]>0){
                baseSignal[8] =  isSelectedZoneB[0];
            }
            if (isSelectedZoneB[1] >0){
                baseSignal[9] =  isSelectedZoneB[1];
            }
            if (isSelectedZoneC[0] >0){
                baseSignal[10] =  isSelectedZoneC[0];
            }
            if (isSelectedZoneC[1] >0){
                baseSignal[11] =  isSelectedZoneC[1];
            }
        }

        Commons.baseSignal = baseSignal;
    }
}
