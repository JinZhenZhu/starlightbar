package com.unitedwebspace.starlightbar.Base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

public abstract class BaseFragment extends Fragment implements View.OnClickListener{

    public BaseActivity _context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void showProgress(){

        _context.showProgress();
    }

    public void CloseProgress(){

        _context.closeProgress();
    }

    public void showToast(String strMsg){

        _context.showToast(strMsg);
    }

    public void showAlert(String strMsg){

        _context.showAlertDialog(strMsg);
    }

    @Override
    public void onClick(View v) {}

    public byte[] makeByteData(String temp){

        String hexString[] =  temp.split(":");
        byte hexByte[] = new byte[hexString.length];

        for (int i = 0; i < hexString.length; i++){
            int tempInt = Integer.parseInt(hexString[i], 16);
            hexByte[i] = (byte)tempInt;
        }

        return hexByte;
    }

    public byte[] makePwdCommand(String temp){

        String strInitPwdData = "01:00:00:00:00:00:00:0:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00";

        int index = 1;
        byte[] newPwd = makeByteData(strInitPwdData);

        char[] strPwd = temp.toCharArray();

        for (int i = 0; i < strPwd.length; i++){
            byte a = (byte) strPwd[i];
            newPwd[index] = a;
            index ++;
        }

        return newPwd;
    }

    public byte[] makeRenameCommand(String temp){

        String strInitPwdData = "02:00:00:00:00:00:00:0:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00";

        int index = 1;
        byte[] newPwd = makeByteData(strInitPwdData);

        char[] strPwd = temp.toCharArray();

        for (int i = 0; i < strPwd.length; i++){
            byte a = (byte) strPwd[i];
            newPwd[index] = a;
            index ++;
        }

        return newPwd;
    }
}
