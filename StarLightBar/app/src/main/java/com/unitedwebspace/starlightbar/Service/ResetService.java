package com.unitedwebspace.starlightbar.Service;

import android.Manifest;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.Common.Constants;

import java.util.Timer;
import java.util.TimerTask;

public class ResetService extends Service {

    final Handler mHandler = new Handler();
    Timer mTimer = null;

    public LocationManager myLocationManager;
    public boolean w_bGpsEnabled, w_bNetworkEnabled;

    public static double myLat = 0;
    public static double myLng = 0;

    public static DevicePolicyManager mDPM;
    public static ComponentName mAdminName;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Initiate DevicePolicyManager.
        mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        // Set DeviceAdminDemo Receiver for active the component with different option
        mAdminName = new ComponentName(this, DeviceAdminDemo.class);

        // scheduling the current position updating task (Asynchronous)
        mTimer = new Timer();
        MyTimeTask myTimeTask = new MyTimeTask();
        mTimer.schedule(myTimeTask, 0, Constants.LOCATION_UPDATE_TIME);

        return START_STICKY; //super.onStartCommand(intent, flags, startId);
    }

    private class MyTimeTask extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    if (Commons.mMainActivity != null &&  !Commons.isLogout){
                        Log.d("aaaa", "aaaaa");
                        Commons.mMainActivity.setResetCommand();
                    }
                }
            });
        }
    }
}
