package com.unitedwebspace.starlightbar.Fragment;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.clj.fastble.conn.BleCharacterCallback;
import com.clj.fastble.exception.BleException;
import com.unitedwebspace.starlightbar.Base.BaseFragment;
import com.unitedwebspace.starlightbar.Bluetooth.BluetoothService;
import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.Main.MainActivity;
import com.unitedwebspace.starlightbar.R;
import com.xw.repo.BubbleSeekBar;

@SuppressLint("ValidFragment")
public class BrightnessFragment extends BaseFragment {

    MainActivity activity;
    BluetoothService mBluetoothService;
    BluetoothGattCharacteristic characteristic;

    ImageView imvLogo, imvDevicePic;
    Button btnSave;
    TextView txvDeviceName, txvBrightness;
    BubbleSeekBar skbBrightness;

    byte[] command = new byte[27];
    byte brightness = 0;

    public BrightnessFragment(MainActivity activity) {
        this.activity = activity;
        command = Commons.baseSignal;
        mBluetoothService = activity.getBluetoothService();
        brightness = Commons.currentBrightness;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_brightness, container, false);

        txvBrightness = (TextView)fragment.findViewById(R.id.txvBrightness);

        imvDevicePic = (ImageView)fragment.findViewById(R.id.imvDevicePic);

        btnSave = (Button)fragment.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        imvLogo = (ImageView)fragment.findViewById(R.id.imvLogo);
        imvLogo.setOnClickListener(this);

        skbBrightness = (BubbleSeekBar)fragment.findViewById(R.id.skbBrightness);
        skbBrightness.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {}

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {}

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                brightness = (byte) progress;
                txvBrightness.setText(String.valueOf(brightness));
            }
        });

        txvDeviceName = (TextView)fragment.findViewById(R.id.txvDeviceName);
        txvDeviceName.setText(Commons.connectedDevice.getOrgName());

        imvDevicePic.setImageResource(getDevicePic(Commons.connectedDevice.getSettingInfo()[2]));

        setValueToRangeBrightness();

        return fragment;
    }

    private void setValueToRangeBrightness() {
        byte[] currentBrightness = new byte[]{Commons.baseSignal[12], Commons.baseSignal[13], Commons.baseSignal[13]};
        if (currentBrightness[0] == currentBrightness[1] && currentBrightness[1] == currentBrightness[2]){
            if (currentBrightness[0] < 25){
                skbBrightness.setProgress(100);
            }else {
                skbBrightness.setProgress(currentBrightness[0]);
            }
        }
    }

    private int getDevicePic(byte whoAmI) {

        int devicePic = R.drawable.ic_bluetooth;

        switch (whoAmI){
            case 0:
                devicePic = R.drawable.slb_m1;
                break;
            case 1:
                devicePic = R.drawable.slb_m2;
                break;
            case 2:
                devicePic = R.drawable.slb_m3;
                break;
            case 3:
                devicePic = R.drawable.slb_m4;
                break;
            case 4:
                devicePic = R.drawable.slb_m5;
                break;
            case 5:
                devicePic = R.drawable.slb_gx_f;
                break;
            case 6:
                devicePic = R.drawable.slb_gx_r;
                break;
            case 7:
                devicePic = R.drawable.slb_gx_w;
                break;
            case 8:
                devicePic = R.drawable.slb_gx_wr;
                break;
        }

        return devicePic;
    }

    @SuppressLint("NewApi")
    private void sendSignal() {
        Commons.baseSignal = command;
        Commons.currentBrightness = brightness;
        characteristic = mBluetoothService.getCharacteristic();
        mBluetoothService.write(characteristic.getService().getUuid().toString(),
                characteristic.getUuid().toString(), command,
                new BleCharacterCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {
                    }

                    @Override
                    public void onFailure(BleException exception) {}

                    @Override
                    public void onInitiatedResult(boolean result) {
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvLogo:
                activity.showHomeFragment();
                break;
            case R.id.btnSave:
                command[12] = brightness;
                command[13] = brightness;
                command[14] = brightness;
                sendSignal();
                break;
        }
    }
}
