package com.unitedwebspace.starlightbar.Common;

public class Constants {
    public static final int SPLASH_TIME = 1000;
    public static final String CONTAINED_DEVICE_NAME = "SLB";
    public static final String DEFAULT_PWD = "SLB_007";
    public static final byte E_STREETMODE = 0;
    public static final byte E_OFF = 3;
    public static final byte E_OEMMODE = 2;
    public static final byte E_DRIVINGMODE = 1;
    public static final int LOCATION_UPDATE_TIME = 10000;
}
