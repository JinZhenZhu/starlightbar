package com.unitedwebspace.starlightbar.Adapter;

import android.bluetooth.BluetoothDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.unitedwebspace.starlightbar.Main.StartupActivity;
import com.unitedwebspace.starlightbar.R;

import java.util.ArrayList;

public class ResultAdapter extends BaseAdapter {
    private StartupActivity activity;
    private ArrayList<BTModel> allData;

    public ResultAdapter(StartupActivity activity) {
        this.activity = activity;
        allData = new ArrayList<>();
    }

    public void refresh(ArrayList<BTModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public BTModel getItem(int position) {
        if (position > allData.size())
            return null;
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomHolder holder;

        if (convertView ==  null){
            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.item_scan_result, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        BTModel btModel = allData.get(position);
        holder.setData(btModel);

        return convertView;
    }

    private class CustomHolder implements View.OnClickListener{
        ImageView imvDevicePic;
        TextView txvDeviceName;

        private void setId (View view){
            imvDevicePic = (ImageView)view.findViewById(R.id.imvDevicePic);
            txvDeviceName = (TextView)view.findViewById(R.id.txvDeviceName);
        }

        private void setData(BTModel model){
            BluetoothDevice device = model.getResult().getDevice();
            txvDeviceName.setText(device.getName());
            if (model.getSettingInfo()[0] == 0 ){
                imvDevicePic.setImageResource(R.drawable.ic_bluetooth);

            }else {
                imvDevicePic.setImageResource(getDevicePic(model.getSettingInfo()[2]));
            }

        }

        private int getDevicePic(byte whoAmI) {

            int devicePic = R.drawable.ic_bluetooth;

            switch (whoAmI){
                case 0:
                    devicePic = R.drawable.slb_m1;
                    break;
                case 1:
                    devicePic = R.drawable.slb_m2;
                    break;
                case 2:
                    devicePic = R.drawable.slb_m3;
                    break;
                case 3:
                    devicePic = R.drawable.slb_m4;
                    break;
                case 4:
                    devicePic = R.drawable.slb_m5;
                    break;
                case 5:
                    devicePic = R.drawable.slb_gx_f;
                    break;
                case 6:
                    devicePic = R.drawable.slb_gx_r;
                    break;
                case 7:
                    devicePic = R.drawable.slb_gx_w;
                    break;
                case 8:
                    devicePic = R.drawable.slb_gx_wr;
                    break;
            }

            return devicePic;
        }

        @Override
        public void onClick(View view) {

        }
    }

}
