package com.unitedwebspace.starlightbar.Adapter;

import com.clj.fastble.data.ScanResult;

import java.io.Serializable;

public class BTModel implements Serializable {
    ScanResult result;
    String orgName = "", orgPwd = "";

    byte[] settingInfo = new byte[27];

    public byte[] getSettingInfo() {
        return settingInfo;
    }

    public void setSettingInfo(byte[] settingInfo) {
        this.settingInfo = settingInfo;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgPwd() {
        return orgPwd;
    }

    public void setOrgPwd(String orgPwd) {
        this.orgPwd = orgPwd;
    }

    public ScanResult getResult() {
        return result;
    }

    public void setResult(ScanResult result) {
        this.result = result;
    }
}
