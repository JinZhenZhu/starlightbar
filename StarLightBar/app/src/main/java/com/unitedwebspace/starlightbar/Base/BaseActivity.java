package com.unitedwebspace.starlightbar.Base;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.R;
import com.unitedwebspace.starlightbar.Utils.ApplicationStateChecker;

import java.util.List;

public abstract class BaseActivity extends AppCompatActivity implements
        Handler.Callback , View.OnClickListener{

    public Context _context = null;

    public Handler _handler = null;

    private ProgressDialog _progressDlg;

    private Vibrator _vibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _context = this;

        _vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        _handler = new Handler(this);
    }

    @Override
    protected void onResume() {
        ApplicationStateChecker.view_resumed(this);
        Log.d("aaaa", "resumed");
        super.onResume();
    }

    @Override
    protected void onStop() {
        ApplicationStateChecker.view_stopped(this);
        if (isAppIsInBackground()){
            Log.d("aaaa", "Stop");
            if (Commons.mMainActivity != null){
                Commons.mMainActivity.finish();
            }
        }
        super.onStop();
    }

    @Override
    protected void onPause() {
        ApplicationStateChecker.view_paused(this);
        Log.d("aaaa", "pause");
        super.onPause();
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onDestroy() {

        closeProgress();

        try {
            if (_vibrator != null)
                _vibrator.cancel();
        } catch (Exception e) {
        }
        _vibrator = null;

        super.onDestroy();
    }


    public void showProgress(boolean cancelable) {

        closeProgress();

        _progressDlg = new ProgressDialog(_context, R.style.MyTheme);
        _progressDlg
                .setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        _progressDlg.setCancelable(cancelable);
        _progressDlg.show();
    }

    public void showProgress() {
        showProgress(false);
    }

    public void closeProgress() {

        if(_progressDlg == null) {
            return;
        }

        _progressDlg.dismiss();
        _progressDlg = null;
    }

    public void showAlertDialog(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }

    /**
     *  show toast
     * @param toast_string
     */
    public void showToast(String toast_string) {

        Toast.makeText(_context, toast_string, Toast.LENGTH_SHORT).show();
    }

    public void showCenterToast(String toast_string) {
        Toast toast = Toast.makeText(_context ,toast_string , Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public void showBottomToast(String toast_string) {

        Toast.makeText(_context, toast_string, Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("MissingPermission")
    public void vibrate() {

        if (_vibrator != null)
            _vibrator.vibrate(500);
    }

    @Override
    public boolean handleMessage(Message msg) {

        switch (msg.what) {

            default:
                break;
        }

        return false;
    }

    @Override
    public void onClick(View v) {}

    public byte[] makeByteData(String temp){

        String hexString[] =  temp.split(":");
        byte hexByte[] = new byte[hexString.length];

        for (int i = 0; i < hexString.length; i++){
            int tempInt = Integer.parseInt(hexString[i], 16);
            hexByte[i] = (byte)tempInt;
        }

        return hexByte;
    }

    public byte[] makePwdCommand(String temp) {

        String strInitPwdData = "03:00:00:00:00:00:00:0:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00";

        int index = 1;
        byte[] newPwd = makeByteData(strInitPwdData);

        char[] strPwd = temp.toCharArray();

        for (int i = 0; i < strPwd.length; i++){
            byte a = (byte) strPwd[i];
            newPwd[index] = a;
            index ++;
        }

        return newPwd;
    }

    private boolean isAppIsInBackground() {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }
}
