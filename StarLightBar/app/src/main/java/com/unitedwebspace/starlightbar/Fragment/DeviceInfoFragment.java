package com.unitedwebspace.starlightbar.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clj.fastble.conn.BleCharacterCallback;
import com.clj.fastble.exception.BleException;
import com.unitedwebspace.starlightbar.Base.BaseFragment;
import com.unitedwebspace.starlightbar.Bluetooth.BluetoothService;
import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.Main.MainActivity;
import com.unitedwebspace.starlightbar.R;
import com.unitedwebspace.starlightbar.SQLite.BTDeviceLocalStore;

@SuppressLint("ValidFragment")
public class DeviceInfoFragment extends BaseFragment {

    MainActivity activity;
    BluetoothService mBluetoothService;
    BluetoothGattCharacteristic characteristic;

    TextView txvDeviceName;
    Button btnSetPassword, btnRename, btnLogout;

    byte[] command = new byte[27];

    public DeviceInfoFragment(MainActivity activity) {
        this.activity = activity;
        mBluetoothService = activity.getBluetoothService();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_device_info, container, false);

        txvDeviceName = (TextView)fragment.findViewById(R.id.txvDeviceName);
        txvDeviceName.setText(Commons.connectedDevice.getOrgName());

        btnSetPassword = (Button)fragment.findViewById(R.id.btnSetPassword);
        btnSetPassword.setOnClickListener(this);

        btnRename = (Button)fragment.findViewById(R.id.btnRename);
        btnRename.setOnClickListener(this);

        btnLogout = (Button)fragment.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(this);

        return fragment;
    }

    private void showChangePwdDialog() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_change_pwd);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvDeviceName = (TextView)dialog.findViewById(R.id.txvDeviceName);
        txvDeviceName.setText(Commons.connectedDevice.getOrgName());

        final EditText edtNewPwd = (EditText)dialog.findViewById(R.id.edtNewPwd);
        final EditText edtConfirmPwd = (EditText)dialog.findViewById(R.id.edtConfirmPwd);

        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newPwd = edtNewPwd.getText().toString().trim();
                String confirmPwd = edtConfirmPwd.getText().toString().trim();

                if (newPwd.equals(confirmPwd)){
                    changePwdInDevice(newPwd);
                    dialog.dismiss();
                }else {
                    activity.showToast("Confirm password");
                }
            }
        });

        Button btnCancel = (Button)dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        Button btnClear = (Button)dialog.findViewById(R.id.btnClear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtNewPwd.setText(""); edtConfirmPwd.setText("");
            }
        });

        dialog.show();
    }

    private void changePwdInDevice(String temp) {
        command = makePwdCommand(temp);
        sendCommand();
        changePwdInDB(temp);
    }

    private void changePwdInDB(String newPassword) {

        BTDeviceLocalStore btDeviceLocalStore =  new BTDeviceLocalStore();

        Commons.mBTBDModel.setOrgPwd(newPassword);

        btDeviceLocalStore.saveBTDevice(Commons.mBTBDModel);
        activity.showToast("Password is changed");
    }

    private void showChangeNameDialog() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_change_name);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView txvDeviceName = (TextView)dialog.findViewById(R.id.txvDeviceName);
        txvDeviceName.setText(Commons.connectedDevice.getOrgName());

        final EditText edtNewName = (EditText)dialog.findViewById(R.id.edtNewName);

        Button btnOk = (Button)dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newName = edtNewName.getText().toString().trim();
                changeNameInDB(newName);
                dialog.dismiss();
            }
        });

        Button btnCancel = (Button)dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        Button btnClear = (Button)dialog.findViewById(R.id.btnClear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtNewName.setText("");
            }
        });

        dialog.show();
    }

    private void changeNameInDB(String newName) {

        BTDeviceLocalStore btDeviceLocalStore =  new BTDeviceLocalStore();

        Commons.mBTBDModel.setOrgName("SLB_" + newName);
        btDeviceLocalStore.saveBTDevice(Commons.mBTBDModel);

        activity.showToast("Device name is changed");

        txvDeviceName.setText(Commons.mBTBDModel.getOrgName());

        Commons.connectedDevice.setOrgName("SLB_" + newName);

        changeNameInDevice("SLB_" + newName);
    }

    private void changeNameInDevice(String name) {
        command = makeRenameCommand(name);
        sendCommand();
    }

    @SuppressLint("NewApi")
    private void sendCommand() {
        characteristic = mBluetoothService.getCharacteristic();

        mBluetoothService.write(characteristic.getService().getUuid().toString(),
                characteristic.getUuid().toString(), command,
                new BleCharacterCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {}

                    @Override
                    public void onFailure(BleException exception) {}

                    @Override
                    public void onInitiatedResult(boolean result) {}
                });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSetPassword:
                showChangePwdDialog();
                break;
            case R.id.btnRename:
                showChangeNameDialog();
                break;
            case R.id.btnLogout:
                String strDisconnectData = "04:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00";
                command = makeByteData(strDisconnectData);
                sendCommand();
                Commons.isLogout = true;
                activity.finish();
                break;
        }
    }
}
