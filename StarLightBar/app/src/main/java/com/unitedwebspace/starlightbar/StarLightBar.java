package com.unitedwebspace.starlightbar;

import android.app.Application;
import android.content.Context;

import com.raizlabs.android.dbflow.config.FlowManager;

public class StarLightBar extends Application {

    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        FlowManager.init(this);
    }
}
