package com.unitedwebspace.starlightbar.SQLite;

import java.util.List;

public interface BTDeviceStore {
    List<BTDBModel> loadBTDevices();

    void saveBTDeviceList(List<BTDBModel> btDeviceList);

    BTDBModel loadBTDevice(int btDeviceId);

    BTDBModel loadBTDevice(String orgName);

    void saveBTDevice(BTDBModel btDevice);

    void deleteBTDevice(BTDBModel btDevice);

    void clean();
}
