package com.unitedwebspace.starlightbar.Fragment;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.clj.fastble.conn.BleCharacterCallback;
import com.clj.fastble.exception.BleException;
import com.unitedwebspace.starlightbar.Base.BaseFragment;
import com.unitedwebspace.starlightbar.Bluetooth.BluetoothService;
import com.unitedwebspace.starlightbar.Common.Commons;
import com.unitedwebspace.starlightbar.Common.Constants;
import com.unitedwebspace.starlightbar.Common.SignalModel;
import com.unitedwebspace.starlightbar.Main.MainActivity;
import com.unitedwebspace.starlightbar.R;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class ZoneFragment extends BaseFragment {

    MainActivity activity;
    BluetoothService mBluetoothService;
    BluetoothGattCharacteristic characteristic;

    TextView txvFunctionName, txvZoneName;
    ImageView imvLogo;
    Button btnEnter;
    RadioGroup rbgContainer;
    ScrollView scvChildContainer, scParentContainer;

    int index = 0, currentIndex = 0;
    byte value = 0;
    byte[] command = new byte[27];
    String functionName = "e_OFF";
    ArrayList<RadioButton> radioButtonList = new ArrayList<>();

    public ZoneFragment(MainActivity activity, int index) {
        this.activity = activity;
        mBluetoothService = activity.getBluetoothService();
        this.index = index;

//        command = Commons.baseSignal;
        if (Commons.currentMode == Constants.E_DRIVINGMODE){
            command = Commons.baseSignal;
        }else {
            SignalModel model = new SignalModel();
            command = model.initSendSignalArray();
            sendDataToBT();
        }
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_zone, container, false);

        scvChildContainer = (ScrollView)fragment.findViewById(R.id.scvChildContainer);

        scParentContainer = (ScrollView)fragment.findViewById(R.id.scParentContainer);
        scParentContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                scvChildContainer.getParent().requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });

        scvChildContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        imvLogo = (ImageView)fragment.findViewById(R.id.imvLogo);
        imvLogo.setOnClickListener(this);

        txvFunctionName = (TextView)fragment.findViewById(R.id.txvFunctionName);
        txvFunctionName.setText(functionName);

        txvZoneName = (TextView)fragment.findViewById(R.id.txvZoneName);
        txvZoneName.setText(Commons.selectedZone);

        btnEnter = (Button)fragment.findViewById(R.id.btnEnter);
        btnEnter.setOnClickListener(this);

        rbgContainer = (RadioGroup)fragment.findViewById(R.id.rbgContainer);
        rbgContainer.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int rbId) {
                switch (rbId){
                    case R.id.rbOff:
                        functionName = "e_OFF";
                        value = SignalModel.BT_ZONE_MODE.e_OFF.asByte();
                        break;
                    case R.id.rbOn:
                        functionName = "e_ON";
                        value = SignalModel.BT_ZONE_MODE.e_ON.asByte();
                        break;
                    case R.id.rbFlash1:
                        functionName = "e_FLASH 1";
                        value = SignalModel.BT_ZONE_MODE.e_FLASH1.asByte();
                        break;
                    case R.id.rbFlash2:
                        functionName = "e_FLASH 2";
                        value = SignalModel.BT_ZONE_MODE.e_FLASH2.asByte();
                        break;
                    case R.id.rbFlash3:
                        functionName = "e_FLASH 3";
                        value = SignalModel.BT_ZONE_MODE.e_FLASH3.asByte();
                        break;
                    case R.id.rbFlash4:
                        functionName = "e_FLASH 4";
                        value = SignalModel.BT_ZONE_MODE.e_FLASH4.asByte();
                        break;
                    case R.id.rbFlash5:
                        functionName = "e_FLASH 5";
                        value = SignalModel.BT_ZONE_MODE.e_FLASH5.asByte();
                        break;
                    case R.id.rbFlashAlt1:
                        functionName = "e_FLASHALT 1";
                        value = SignalModel.BT_ZONE_MODE.e_FLASHALT1.asByte();
                        break;
                    case R.id.rbFlashAlt2:
                        functionName = "e_FLASHALT 2";
                        value = SignalModel.BT_ZONE_MODE.e_FLASHALT2.asByte();
                        break;
                    case R.id.rbFlashAlt3:
                        functionName = "e_FLASHALT 3";
                        value = SignalModel.BT_ZONE_MODE.e_FLASHALT3.asByte();
                        break;
                    case R.id.rbFlashAlt4:
                        functionName = "e_FLASHALT 4";
                        value = SignalModel.BT_ZONE_MODE.e_FLASHALT4.asByte();
                        break;
                    case R.id.rbFlashAlt5:
                        functionName = "e_FLASHALT 5";
                        value = SignalModel.BT_ZONE_MODE.e_FLASHALT5.asByte();
                        break;
                    case R.id.rbFlashAlta1:
                        functionName = "e_FLASHALTA 1";
                        value = SignalModel.BT_ZONE_MODE.e_FLASHALTA1.asByte();
                        break;
                    case R.id.rbFlashAlta2:
                        functionName = "e_FLASHALTA 2";
                        value = SignalModel.BT_ZONE_MODE.e_FLASHALTA2.asByte();
                        break;
                    case R.id.rbFlashAlta3:
                        functionName = "e_FLASHALTA 3";
                        value = SignalModel.BT_ZONE_MODE.e_FLASHALTA3.asByte();
                        break;
                    case R.id.rbFlashAlta4:
                        functionName = "e_FLASHALTA 4";
                        value = SignalModel.BT_ZONE_MODE.e_FLASHALTA4.asByte();
                        break;
                    case R.id.rbFlashAlta5:
                        functionName = "e_FLASHALTA 5";
                        value = SignalModel.BT_ZONE_MODE.e_FLASHALTA5.asByte();
                        break;
                    case R.id.rbRandom1:
                        functionName = "e_RANDOM 1";
                        value = SignalModel.BT_ZONE_MODE.e_RANDOM1.asByte();
                        break;
                    case R.id.rbRandom2:
                        functionName = "e_RANDOM 2";
                        value = SignalModel.BT_ZONE_MODE.e_RANDOM2.asByte();
                        break;
                    case R.id.rbRandom3:
                        functionName = "e_RANDOM 3";
                        value = SignalModel.BT_ZONE_MODE.e_RANDOM3.asByte();
                        break;
                    case R.id.rbRandom4:
                        functionName = "e_RANDOM 4";
                        value = SignalModel.BT_ZONE_MODE.e_RANDOM4.asByte();
                        break;
                    case R.id.rbRandom5:
                        functionName = "e_RANDOM 5";
                        value = SignalModel.BT_ZONE_MODE.e_RANDOM5.asByte();
                        break;
                }
            }
        });

        setRadioButton(fragment);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                setInitValueToRadioButtonGroup();

            }
        }, 500);

        return fragment;
    }

    private void setInitValueToRadioButtonGroup() {
        byte[] isSelectedZoneA = new byte[]{command[6], command[7]};
        byte[] isSelectedZoneB = new byte[]{command[8], command[9]};
        byte[] isSelectedZoneC = new byte[]{command[10], command[11]};

        if (index == 0 && isSelectedZoneA[0] >0 && isSelectedZoneA[1] >0 && isSelectedZoneA[0] == isSelectedZoneA[1]){
            currentIndex =  isSelectedZoneA[0];
        }else if (index == 1 && isSelectedZoneB[0] >0 && isSelectedZoneB[1] >0 && isSelectedZoneB[0] == isSelectedZoneB[1]){
            currentIndex =  isSelectedZoneB[0];
        }else if (index == 2 && isSelectedZoneC[0] >0 && isSelectedZoneC[1] >0 && isSelectedZoneC[0] == isSelectedZoneC[1]){
            currentIndex =  isSelectedZoneC[0];
        }else if (index == 7 && isSelectedZoneA[0] >0){
            currentIndex =  isSelectedZoneA[0];
        }else if (index == 8 && isSelectedZoneA[1] >0){
            currentIndex =  isSelectedZoneA[1];
        }else if (index == 9 && isSelectedZoneB[0]>0){
            currentIndex =  isSelectedZoneB[0];
        }else if (index == 10 && isSelectedZoneB[1] >0){
            currentIndex =  isSelectedZoneB[1];
        }else if (index == 11 && isSelectedZoneC[0] >0){
            currentIndex =  isSelectedZoneC[0];
        }else if (index == 12 && isSelectedZoneC[1] >0){
            currentIndex =  isSelectedZoneC[1];
        }

        setRadioButton();
    }

    private void setRadioButton (){
        radioButtonList.get(currentIndex).setChecked(true);
        scvChildContainer.smoothScrollTo(0, (int) radioButtonList.get(currentIndex).getY());
    }

    private void setRadioButton(View fragment) {
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbOff));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbOn));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlash1));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlash2));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlash3));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlash4));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlash5));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlashAlt1));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlashAlt2));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlashAlt3));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlashAlt4));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlashAlt5));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlashAlta1));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlashAlta2));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlashAlta3));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlashAlta4));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbFlashAlta5));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbRandom1));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbRandom2));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbRandom3));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbRandom4));
        radioButtonList.add((RadioButton)fragment.findViewById(R.id.rbRandom5));
    }

    @SuppressLint("NewApi")
    private void sendDataToBT() {

        Commons.currentMode = Constants.E_DRIVINGMODE;
        Commons.baseSignal = command;

        characteristic = mBluetoothService.getCharacteristic();
        mBluetoothService.write(characteristic.getService().getUuid().toString(),
                characteristic.getUuid().toString(), command,
                new BleCharacterCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {}

                    @Override
                    public void onFailure(BleException exception) {
                        showToast("Failure");
                    }

                    @Override
                    public void onInitiatedResult(boolean result) {}
                });
    }

    private void makeCommandForABCZones() {

        command[1] = SignalModel.BT_MODE.e_DRIVINGMODE.asByte();

        switch (index){
            case 0:
                command[6] = value; command[7] = value;
                break;
            case 1:
                command[8] = value; command[9] = value;
                break;
            case 2:
                command[10] = value; command[11] = value;
                break;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.imvLogo:
                activity.showHomeFragment1();
                break;
            case R.id.btnEnter:
                txvFunctionName.setText(functionName);
                if (index < 7){
                    makeCommandForABCZones();
                }else {
                    command[1] = SignalModel.BT_MODE.e_DRIVINGMODE.asByte();
                    command[index - 1] = value;
                }
                sendDataToBT();
                break;
        }
    }
}
